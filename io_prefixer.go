package main

import (
	"io"
	"log"
	"strings"
)

// Print, line-by-line, a prefixed version of the input stream.
// Splits on newlines and ignores blank lines.
type IOPrefixer struct {
	label string
}

func (l *IOPrefixer) Write(in []byte) (int, error) {
	lines := strings.Split(string(in), "\n")
	for _, line := range lines {
		if line == "" {
			continue
		}

		log.Printf("%s | %s\n", l.label, line)
	}

	return len(in), nil
}

// Initialize and return a new IOPrefixer
func NewIOPrefixer(label string) io.Writer {
	return &IOPrefixer{
		label: label,
	}
}
