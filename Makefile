PACKAGE=miso

.PHONY: build
build:
	@mkdir -p $(CURDIR)/bin
	@cd $(CURDIR) && go build -v -o bin/$(PACKAGE) && echo "Build succeeded, see bin/$(PACKAGE)"

.PHONY: install
install:
	@cd $(CURDIR) && go install -v && echo "Install succeeded"
