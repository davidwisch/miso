package main

import (
	"fmt"
	"log"
	"miso/cli"
	"os/exec"
	"strconv"
	"strings"
	"sync"
	"syscall"
)

// All commands executed through /bin/sh.
// This allows complex commands that include pipes and redirections to be used
const Shell = "/bin/sh"

var colors = []int{
	11,  // yellow
	10,  // green
	9,   // red
	14,  // teal
	5,   // purple
	4,   //blue
	13,  // pink
	208, // orange
}

func wrapWithColor(index int, msg string) string {
	maxColors := len(colors)
	if index > maxColors {
		log.Printf("Only %d unique colors, wrapping around\n", maxColors)
	}

	index = index % maxColors

	// requires color-capable terminal
	return fmt.Sprintf("\x1b[38;5;%dm%s\x1b[0m", colors[index], msg)
}

// Create an exec.Cmd with stdout/stderr streams set to
// an IOPrefixer and stdin set to the command string.
func createLabeledCmd(label, cmdStr string) *exec.Cmd {
	labeler := NewIOPrefixer(label)

	cmd := exec.Command(Shell, "-f", "-")
	cmd.Stdin = strings.NewReader(cmdStr)
	cmd.Stdout = labeler
	cmd.Stderr = labeler

	return cmd
}

// Execute the command, wait for the results, and return the exit code
func runCmd(cmd *exec.Cmd) int {
	var exitCode int
	err := cmd.Run()
	if err != nil {
		waitStatus := cmd.ProcessState.Sys().(syscall.WaitStatus) // assuming *nix
		exitCode = waitStatus.ExitStatus()
	}

	// use the commands prefixed stream to get labeling
	cmd.Stdout.Write([]byte(fmt.Sprintf("Process %d exited with code %d", cmd.Process.Pid, exitCode)))

	return exitCode
}

func main() {
	log.SetFlags(0)
	opts := cli.Parse()

	var wg sync.WaitGroup
	wg.Add(len(opts.Cmds))

	for i, cmd := range opts.Cmds {
		label := cmd.Label
		if label == "" {
			label = strconv.Itoa(i)
		}

		c := createLabeledCmd(wrapWithColor(i, label), cmd.Cmd)

		log.Printf("Starting '%s'...\n", label)
		go func(cmd *exec.Cmd) {
			defer wg.Done()
			runCmd(cmd)
		}(c)
	}

	wg.Wait()
	log.Println("All processes have exited.")
}
