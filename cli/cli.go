package cli

import (
	"flag"
	"fmt"
	"log"
	"os"
	"strings"
)

const help = `miso - Multi In Single Out

Combine stdout/stderr from multiple processes into a single colorizd labeled
stdout steram.

Usage: miso [-v] -cmd [label:]cmd [-cmd ...]

Options:
`

const helpDetails = `
Examples:
	miso -cmd "apache:tail -f /path/to/apache/log"
		Tail a single apache log

	miso -cmd "pod1:kubectl logs -f pod1_name" -cmd "pod2:kubectl logs -f pod2_name"
		Tail multiple Kubernetes pods

Colors:
	miso selects different colors for each label. The colors are not
	configurable, but they are determnistic. If more commands are
	specified than the available color palette, colors are repeated.

Output:

	The output from a command of the following format:

		miso -cmd "tail1:tail -f /path/to/file" -cmd "tail2:/path/to/file"

	Would look similar to the following:

		tail1 | line 1
		tail1 | line2
		tail2 | line1
		tail1 | line3
		tail2 | line2
		...
`

func init() {
	flag.Usage = func() {
		log.Print(help)
		flag.PrintDefaults()
		log.Print(helpDetails)
	}
}

// Seperates the label from the command component of the input
const CmdLabelSep = ":"

// Label & command pairing
type CmdDef struct {
	Label string
	Cmd   string
}

// Config options, parsed from CLI
type CliOpts struct {
	Cmds []*CmdDef
}

// Used to accept multiple of the same CLI flag
type arrayFlags []string

func (a *arrayFlags) String() string {
	return fmt.Sprintf("[]arrayFlag (num=%d)", len(*a))
}
func (a *arrayFlags) Set(value string) error {
	*a = append(*a, value)
	return nil
}

// Extract a comment definition struct from an input string
// Expected input format: [label]:[cmd]. Label is optional, and if misssing
// or blank, will be calcualted later.
func cmdDefFromStr(cmd string) *CmdDef {
	pieces := strings.SplitN(cmd, CmdLabelSep, 2)

	var label string
	var cmdStr string

	if len(pieces) == 1 {
		label = ""
		cmdStr = pieces[0]
	} else {
		label = pieces[0]
		cmdStr = pieces[1]
	}

	return &CmdDef{
		Label: label,
		Cmd:   cmdStr,
	}
}

// Parse argv
func Parse() *CliOpts {
	var cmds arrayFlags

	opts := &CliOpts{
		Cmds: []*CmdDef{},
	}

	flag.Var(&cmds, "cmd", "Commands to run. Can specify multiple. Format='[label:]cmd'. If 'label' is omited, the index of the command is used. All commands are executed via /bin/sh")
	version := flag.Bool("v", false, "Print the version and exit")

	flag.Parse()

	for _, cmd := range cmds {
		def := cmdDefFromStr(cmd)
		opts.Cmds = append(opts.Cmds, def)
	}

	// conditions that will trigger immediate exit
	switch {
	case *version:
		log.Println(Version)
		os.Exit(0)
	case len(opts.Cmds) == 0:
		flag.Usage()
		os.Exit(2)
	}

	return opts
}
