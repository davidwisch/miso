# miso (Multi In Single Out)

## Help
```
miso - Multi In Single Out

Combine stdout/stderr from multiple processes into a single colorized labeled
stdout steram.

Usage: miso [-v] -cmd [label:]cmd [-cmd ...]

Options:
  -cmd value
    	Commands to run. Can specify multiple. Format='[label:]cmd'. If 'label' is omited, the index of the command is used. All commands are executed via /bin/sh
  -v	Print the version and exit

Examples:
	miso -cmd "apache:tail -f /path/to/apache/log"
		Tail a single apache log

	miso -cmd "pod1:kubectl logs -f pod1_name" -cmd "pod2:kubectl logs -f pod2_name"
		Tail multiple Kubernetes pods

Colors:
	miso selects different colors for each label. The colors are not
	configurable, but they are determnistic. If more commands are
	specified than the available color palette, colors are repeated.

Output:

	The output from a command of the following format:

		miso -cmd "tail1:tail -f /path/to/file" -cmd "tail2:/path/to/file"

	Would look similar to the following:

		tail1 | line 1
		tail1 | line2
		tail2 | line1
		tail1 | line3
		tail2 | line2
		...
```

## Limitations

* Linux only

## Watch out

* I suspect there could be issues if command output isn't line-buffered, but havent tested specifically
